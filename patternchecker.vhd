library work;
    use work.all;
library ieee;
    use ieee.std_logic_1164.all;


entity patternchecker is -- Device Under test
    port (
                                                       -- debugging only
    dip1 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 1 clock cycle (for debugging purpose only)
    dip2 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 2 clock cycles (for debugging purpose only)
    dip3 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 3 clock cycles (for debugging purpose only)
    dip4 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 4 clock cycles (for debugging purpose only)
    dip5 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 5 clock cycles (for debugging purpose only)
    dip6 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 6 clock cycles (for debugging purpose only)
    --
    do : out std_logic_vector( 1 downto 0 ); -- Data Out
    mrk : out std_logic; -- MaRK of 1st data following pattern
    --
    di : in std_logic_vector( 1 downto 0 ); -- Data In
    --
    clk : in std_logic; -- CLocK
    nres : in std_logic -- Not RESet ; low active reset
    );--]port
end entity patternchecker;


architecture rtl_mealy of patternchecker is

    signal di_ns : std_logic_vector( 1 downto 0 );
    signal di_cs : std_logic_vector( 1 downto 0 ) := (others=>'0');
    signal state_ns : std_logic_vector( 1 downto 0 );
    signal state_cs : std_logic_vector( 1 downto 0 ) := (others=>'0');
    signal mrk_ns : std_logic;
    signal mrk_cs : std_logic := '0';
    signal dip1_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal dip2_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal dip3_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal dip4_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal dip5_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal dip6_cs : std_logic_vector (1 downto 0) := (others=>'0');




begin

    di_ns <= di;

reg:
    process ( clk ) is
    begin
        if  clk='1' and clk'event  then
            if  nres='0' then
                state_cs <= (others=>'0');
                di_cs <= (others=>'0');
                mrk_cs <= '0';
            else
                state_cs <= state_ns;
                di_cs <= di_ns;
                mrk_cs <= mrk_ns;
            end if;
        end if;
    end process reg;


shiftreg:
    process ( clk ) is
    begin
        if  clk='1' and clk'event  then
            if  nres='0' then
                dip1_cs <= (others=>'0');
                dip2_cs <= (others=>'0');
                dip3_cs <= (others=>'0');
                dip4_cs <= (others=>'0');
                dip5_cs <= (others=>'0');
                dip6_cs <= (others=>'0');
            else
                dip1_cs <= di_cs;
                dip2_cs <= dip1_cs;
                dip3_cs <= dip2_cs;
                dip4_cs <= dip3_cs;
                dip5_cs <= dip4_cs;
                dip6_cs <= dip5_cs;
            end if;
        end if;
    end process shiftreg;



SN:
    process (di_cs, state_cs) is
        variable di_v : std_logic_vector ( 1 downto 0 );
        variable state_v : std_logic_vector ( 1 downto 0 );
        variable mrk_v : std_logic;
    begin

    di_v := di_cs;
    state_v := state_cs;
    mrk_v := '0';

        case state_v is

            when "00" =>
                case di_v is
                    when "11" =>    state_v := "01";     mrk_v := '0';
                    when others =>  state_v := "00";     mrk_v := '0';
                end case;

            when "01" =>
                case di_v is
                    when "11" =>    state_v := "10";     mrk_v := '0';
                    when others =>  state_v := "00";     mrk_v := '0';
                end case;

            when "10" =>
                case di_v is
                    when "00" =>    state_v := "11";    mrk_v := '0';
                    when "11" =>    state_v := "10";    mrk_v := '0';
                    when others =>  state_v := "00";    mrk_v := '0';
                end case;

            when "11" =>
                case di_v is
                    when "00" =>    state_v := "00";    mrk_v := '1';
                    when "11" =>    state_v := "01";    mrk_v := '0';
                    when others =>  state_v := "00";    mrk_v := '0';
                end case;

            when others =>
                state_v := "00";    -- RESET-Zustand
                mrk_v := '0';
        end case;

    mrk_ns <= mrk_v;
    state_ns <= state_v;
    end process SN;

    mrk <= mrk_cs;
    do <= di_cs;
    dip1 <= dip1_cs;
    dip2 <= dip2_cs;
    dip3 <= dip3_cs;
    dip4 <= dip4_cs;
    dip5 <= dip5_cs;
    dip6 <= dip6_cs;


end architecture rtl_mealy;

architecture rtl_moore of patternchecker is

    signal di_ns : std_logic_vector( 1 downto 0 );
    signal di_cs : std_logic_vector( 1 downto 0 ) := (others=>'0');
    signal do_cs : std_logic_vector( 1 downto 0 ) := (others=>'0');
    signal do_ns : std_logic_vector( 1 downto 0 );
    signal state_ns : std_logic_vector( 2 downto 0 );
    signal state_cs : std_logic_vector( 2 downto 0 ) := (others=>'0');
    signal dip1_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal dip2_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal dip3_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal dip4_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal dip5_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal dip6_cs : std_logic_vector (1 downto 0) := (others=>'0');
    signal mrk_ns : std_logic;
    signal mrk_cs : std_logic := '0';



begin

    di_ns <= di;

reg:
    process ( clk ) is
    begin
        if  clk='1' and clk'event  then
            if  nres='0' then
                state_cs <= (others=>'0');
                di_cs <= (others=>'0');
                mrk_cs <= '0';
                do_cs <= (others=>'0');
            else
                state_cs <= state_ns;
                di_cs <= di_ns;
                mrk_cs <= mrk_ns;
                do_cs <= di_cs;
            end if;
        end if;
    end process reg;


shiftreg:
    process ( clk ) is
    begin
        if  clk='1' and clk'event  then
            if  nres='0' then
                dip1_cs <= (others=>'0');
                dip2_cs <= (others=>'0');
                dip3_cs <= (others=>'0');
                dip4_cs <= (others=>'0');
                dip5_cs <= (others=>'0');
                dip6_cs <= (others=>'0');
            else
                dip1_cs <= di_cs;
                dip2_cs <= dip1_cs;
                dip3_cs <= dip2_cs;
                dip4_cs <= dip3_cs;
                dip5_cs <= dip4_cs;
                dip6_cs <= dip5_cs;
            end if;
        end if;
    end process shiftreg;


UESN:
    process (di_cs, state_cs) is
        variable di_v : std_logic_vector ( 1 downto 0 );
        variable state_v : std_logic_vector ( 2 downto 0 );
    begin

        di_v := di_cs;
        state_v := state_cs;

        case state_v is

            when "000" =>
                case di_v is
                    when "11" =>    state_v := "001";
                    when others =>  state_v := "000";
                end case;

            when "001" =>
                case di_v is
                    when "11" =>    state_v := "010";
                    when others =>  state_v := "000";
                end case;

            when "010" =>
                case di_v is
                    when "00" =>    state_v := "011";
                    when "11" =>    state_v := "010";
                    when others =>  state_v := "000";
                end case;

            when "011" =>
                case di_v is
                    when "00" =>    state_v := "100";
                    when "11" =>    state_v := "001";
                    when others =>  state_v := "000";
                end case;

            when "100" =>
                case di_v is
                    when "11" =>    state_v := "001";
                    when others =>  state_v := "000";
                end case;

            when others =>
                state_v := "000";    -- RESET-Zustand
        end case;

        state_ns <= state_v;
    end process UESN;

ASN:
    process (state_cs) is
        variable state_v : std_logic_vector ( 2 downto 0 );
        variable mrk_v : std_logic;
    begin

        state_v := state_cs;

        case state_v is
            when "100" =>   mrk_v := '1';
            when others =>  mrk_v := '0';
        end case;

        mrk_ns <= mrk_v;
    end process ASN;

    do <= do_cs;
    mrk <= mrk_cs;
    dip1 <= dip1_cs;
    dip2 <= dip2_cs;
    dip3 <= dip3_cs;
    dip4 <= dip4_cs;
    dip5 <= dip5_cs;
    dip6 <= dip6_cs;

end architecture rtl_moore;