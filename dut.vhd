library work;
    use work.all;
library ieee;
    use ieee.std_logic_1164.all;

entity dut is -- Device Under test
    port (
                                                       -- debugging only
    dip1 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 1 clock cycle (for debugging purpose only)
    dip2 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 2 clock cycles (for debugging purpose only)
    dip3 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 3 clock cycles (for debugging purpose only)
    dip4 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 4 clock cycles (for debugging purpose only)
    dip5 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 5 clock cycles (for debugging purpose only)
    dip6 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 6 clock cycles (for debugging purpose only)
    --
    do : out std_logic_vector( 1 downto 0 ); -- Data Out
    mrk : out std_logic; -- MaRK of 1st data following pattern
    --
    di : in std_logic_vector( 1 downto 0 ); -- Data In
    --
    clk : in std_logic; -- CLocK
    nres : in std_logic -- Not RESet ; low active reset
    );--]port
end entity dut;


architecture beh of dut is

    component patternchecker is
        port (
                dip1 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 1 clock cycle (for debugging purpose only)
                dip2 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 2 clock cycles (for debugging purpose only)
                dip3 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 3 clock cycles (for debugging purpose only)
                dip4 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 4 clock cycles (for debugging purpose only)
                dip5 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 5 clock cycles (for debugging purpose only)
                dip6 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 6 clock cycles (for debugging purpose only)
                do : out std_logic_vector( 1 downto 0 ); -- Data Out
                mrk : out std_logic; -- MaRK of 1st data following pattern
                --
                di : in std_logic_vector( 1 downto 0 ); -- Data In
                --
                clk : in std_logic; -- CLocK
                nres : in std_logic -- Not RESet ; low active reset
            );
    end component patternchecker;
    for all : patternchecker use entity work.patternchecker( rtl_moore );

begin

patternchecker_i : patternchecker
        port map (
            di => di,
            clk => clk,
            nres => nres,
            do => do,
            mrk => mrk,

            dip1 => dip1,
            dip2 => dip2,
            dip3 => dip3,
            dip4 => dip4,
            dip5 => dip5,
            dip6 => dip6

        )--]port
    ;--]sg_i


end architecture beh;