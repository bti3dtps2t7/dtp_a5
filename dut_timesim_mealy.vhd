--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: dut_timesim.vhd
-- /___/   /\     Timestamp: Fri Jan 15 14:09:13 2016
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm dut -w -dir netgen/fit -ofmt vhdl -sim dut.nga dut_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: dut.nga
-- Output file	: D:\DTP\S2T7\dtp_a5\ISE\ise_14x4\netgen\fit\dut_timesim.vhd
-- # of Entities	: 1
-- Design Name	: dut.nga
-- Xilinx	: C:\Xilinx\14.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

architecture Structure_mealy of dut is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal di_0_II_UIM_3 : STD_LOGIC; 
  signal di_1_II_UIM_5 : STD_LOGIC; 
  signal nres_II_UIM_7 : STD_LOGIC; 
  signal dip1_0_MC_Q_9 : STD_LOGIC; 
  signal dip1_1_MC_Q_11 : STD_LOGIC; 
  signal dip2_0_MC_Q_13 : STD_LOGIC; 
  signal dip2_1_MC_Q_15 : STD_LOGIC; 
  signal dip3_0_MC_Q_17 : STD_LOGIC; 
  signal dip3_1_MC_Q_19 : STD_LOGIC; 
  signal dip4_0_MC_Q_21 : STD_LOGIC; 
  signal dip4_1_MC_Q_23 : STD_LOGIC; 
  signal dip5_0_MC_Q_25 : STD_LOGIC; 
  signal dip5_1_MC_Q_27 : STD_LOGIC; 
  signal dip6_0_MC_Q_29 : STD_LOGIC; 
  signal dip6_1_MC_Q_31 : STD_LOGIC; 
  signal do_0_MC_Q_33 : STD_LOGIC; 
  signal do_1_MC_Q_35 : STD_LOGIC; 
  signal mrk_MC_Q_37 : STD_LOGIC; 
  signal dip1_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip1_0_MC_UIM_39 : STD_LOGIC; 
  signal dip1_0_MC_D_40 : STD_LOGIC; 
  signal Gnd_41 : STD_LOGIC; 
  signal Vcc_42 : STD_LOGIC; 
  signal dip1_0_MC_D1_43 : STD_LOGIC; 
  signal dip1_0_MC_D2_44 : STD_LOGIC; 
  signal do_0_MC_UIM_45 : STD_LOGIC; 
  signal do_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal do_0_MC_D_47 : STD_LOGIC; 
  signal do_0_MC_D1_48 : STD_LOGIC; 
  signal do_0_MC_D2_49 : STD_LOGIC; 
  signal dip1_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip1_1_MC_UIM_51 : STD_LOGIC; 
  signal dip1_1_MC_D_52 : STD_LOGIC; 
  signal dip1_1_MC_D1_53 : STD_LOGIC; 
  signal dip1_1_MC_D2_54 : STD_LOGIC; 
  signal do_1_MC_UIM_55 : STD_LOGIC; 
  signal do_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal do_1_MC_D_57 : STD_LOGIC; 
  signal do_1_MC_D1_58 : STD_LOGIC; 
  signal do_1_MC_D2_59 : STD_LOGIC; 
  signal dip2_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip2_0_MC_UIM_61 : STD_LOGIC; 
  signal dip2_0_MC_D_62 : STD_LOGIC; 
  signal dip2_0_MC_D1_63 : STD_LOGIC; 
  signal dip2_0_MC_D2_64 : STD_LOGIC; 
  signal dip2_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip2_1_MC_UIM_66 : STD_LOGIC; 
  signal dip2_1_MC_D_67 : STD_LOGIC; 
  signal dip2_1_MC_D1_68 : STD_LOGIC; 
  signal dip2_1_MC_D2_69 : STD_LOGIC; 
  signal dip3_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip3_0_MC_UIM_71 : STD_LOGIC; 
  signal dip3_0_MC_D_72 : STD_LOGIC; 
  signal dip3_0_MC_D1_73 : STD_LOGIC; 
  signal dip3_0_MC_D2_74 : STD_LOGIC; 
  signal dip3_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip3_1_MC_UIM_76 : STD_LOGIC; 
  signal dip3_1_MC_D_77 : STD_LOGIC; 
  signal dip3_1_MC_D1_78 : STD_LOGIC; 
  signal dip3_1_MC_D2_79 : STD_LOGIC; 
  signal dip4_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip4_0_MC_UIM_81 : STD_LOGIC; 
  signal dip4_0_MC_D_82 : STD_LOGIC; 
  signal dip4_0_MC_D1_83 : STD_LOGIC; 
  signal dip4_0_MC_D2_84 : STD_LOGIC; 
  signal dip4_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip4_1_MC_UIM_86 : STD_LOGIC; 
  signal dip4_1_MC_D_87 : STD_LOGIC; 
  signal dip4_1_MC_D1_88 : STD_LOGIC; 
  signal dip4_1_MC_D2_89 : STD_LOGIC; 
  signal dip5_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip5_0_MC_UIM_91 : STD_LOGIC; 
  signal dip5_0_MC_D_92 : STD_LOGIC; 
  signal dip5_0_MC_D1_93 : STD_LOGIC; 
  signal dip5_0_MC_D2_94 : STD_LOGIC; 
  signal dip5_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip5_1_MC_UIM_96 : STD_LOGIC; 
  signal dip5_1_MC_D_97 : STD_LOGIC; 
  signal dip5_1_MC_D1_98 : STD_LOGIC; 
  signal dip5_1_MC_D2_99 : STD_LOGIC; 
  signal dip6_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip6_0_MC_D_101 : STD_LOGIC; 
  signal dip6_0_MC_D1_102 : STD_LOGIC; 
  signal dip6_0_MC_D2_103 : STD_LOGIC; 
  signal dip6_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip6_1_MC_D_105 : STD_LOGIC; 
  signal dip6_1_MC_D1_106 : STD_LOGIC; 
  signal dip6_1_MC_D2_107 : STD_LOGIC; 
  signal mrk_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal mrk_MC_D_109 : STD_LOGIC; 
  signal mrk_MC_D1_110 : STD_LOGIC; 
  signal mrk_MC_D2_111 : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd1_112 : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd2_113 : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd1_MC_Q : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd1_MC_D_115 : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd1_MC_D1_116 : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd1_MC_D2_117 : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_118 : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_119 : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd2_MC_Q : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd2_MC_D_121 : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd2_MC_D1_122 : STD_LOGIC; 
  signal patternchecker_i_state_cs_FSM_FFd2_MC_D2_123 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_mrk_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_mrk_MC_D1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_mrk_MC_D1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN2 : STD_LOGIC; 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  di_0_II_UIM : X_BUF
    port map (
      I => di(0),
      O => di_0_II_UIM_3
    );
  di_1_II_UIM : X_BUF
    port map (
      I => di(1),
      O => di_1_II_UIM_5
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_7
    );
  dip1_0_Q : X_BUF
    port map (
      I => dip1_0_MC_Q_9,
      O => dip1(0)
    );
  dip1_1_Q : X_BUF
    port map (
      I => dip1_1_MC_Q_11,
      O => dip1(1)
    );
  dip2_0_Q : X_BUF
    port map (
      I => dip2_0_MC_Q_13,
      O => dip2(0)
    );
  dip2_1_Q : X_BUF
    port map (
      I => dip2_1_MC_Q_15,
      O => dip2(1)
    );
  dip3_0_Q : X_BUF
    port map (
      I => dip3_0_MC_Q_17,
      O => dip3(0)
    );
  dip3_1_Q : X_BUF
    port map (
      I => dip3_1_MC_Q_19,
      O => dip3(1)
    );
  dip4_0_Q : X_BUF
    port map (
      I => dip4_0_MC_Q_21,
      O => dip4(0)
    );
  dip4_1_Q : X_BUF
    port map (
      I => dip4_1_MC_Q_23,
      O => dip4(1)
    );
  dip5_0_Q : X_BUF
    port map (
      I => dip5_0_MC_Q_25,
      O => dip5(0)
    );
  dip5_1_Q : X_BUF
    port map (
      I => dip5_1_MC_Q_27,
      O => dip5(1)
    );
  dip6_0_Q : X_BUF
    port map (
      I => dip6_0_MC_Q_29,
      O => dip6(0)
    );
  dip6_1_Q : X_BUF
    port map (
      I => dip6_1_MC_Q_31,
      O => dip6(1)
    );
  do_0_Q : X_BUF
    port map (
      I => do_0_MC_Q_33,
      O => do(0)
    );
  do_1_Q : X_BUF
    port map (
      I => do_1_MC_Q_35,
      O => do(1)
    );
  mrk_38 : X_BUF
    port map (
      I => mrk_MC_Q_37,
      O => mrk
    );
  dip1_0_MC_Q : X_BUF
    port map (
      I => dip1_0_MC_Q_tsimrenamed_net_Q,
      O => dip1_0_MC_Q_9
    );
  dip1_0_MC_UIM : X_BUF
    port map (
      I => dip1_0_MC_Q_tsimrenamed_net_Q,
      O => dip1_0_MC_UIM_39
    );
  dip1_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip1_0_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip1_0_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip1_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_41
    );
  Vcc : X_ONE
    port map (
      O => Vcc_42
    );
  dip1_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip1_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip1_0_MC_D_IN1,
      O => dip1_0_MC_D_40
    );
  dip1_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip1_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip1_0_MC_D1_IN1,
      O => dip1_0_MC_D1_43
    );
  dip1_0_MC_D2 : X_ZERO
    port map (
      O => dip1_0_MC_D2_44
    );
  do_0_MC_Q : X_BUF
    port map (
      I => do_0_MC_Q_tsimrenamed_net_Q,
      O => do_0_MC_Q_33
    );
  do_0_MC_UIM : X_BUF
    port map (
      I => do_0_MC_Q_tsimrenamed_net_Q,
      O => do_0_MC_UIM_45
    );
  do_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_do_0_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_do_0_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => do_0_MC_Q_tsimrenamed_net_Q
    );
  do_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_do_0_MC_D_IN0,
      I1 => NlwBufferSignal_do_0_MC_D_IN1,
      O => do_0_MC_D_47
    );
  do_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_do_0_MC_D1_IN0,
      I1 => NlwBufferSignal_do_0_MC_D1_IN1,
      O => do_0_MC_D1_48
    );
  do_0_MC_D2 : X_ZERO
    port map (
      O => do_0_MC_D2_49
    );
  dip1_1_MC_Q : X_BUF
    port map (
      I => dip1_1_MC_Q_tsimrenamed_net_Q,
      O => dip1_1_MC_Q_11
    );
  dip1_1_MC_UIM : X_BUF
    port map (
      I => dip1_1_MC_Q_tsimrenamed_net_Q,
      O => dip1_1_MC_UIM_51
    );
  dip1_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip1_1_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip1_1_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip1_1_MC_Q_tsimrenamed_net_Q
    );
  dip1_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip1_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip1_1_MC_D_IN1,
      O => dip1_1_MC_D_52
    );
  dip1_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip1_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip1_1_MC_D1_IN1,
      O => dip1_1_MC_D1_53
    );
  dip1_1_MC_D2 : X_ZERO
    port map (
      O => dip1_1_MC_D2_54
    );
  do_1_MC_Q : X_BUF
    port map (
      I => do_1_MC_Q_tsimrenamed_net_Q,
      O => do_1_MC_Q_35
    );
  do_1_MC_UIM : X_BUF
    port map (
      I => do_1_MC_Q_tsimrenamed_net_Q,
      O => do_1_MC_UIM_55
    );
  do_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_do_1_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_do_1_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => do_1_MC_Q_tsimrenamed_net_Q
    );
  do_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_do_1_MC_D_IN0,
      I1 => NlwBufferSignal_do_1_MC_D_IN1,
      O => do_1_MC_D_57
    );
  do_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_do_1_MC_D1_IN0,
      I1 => NlwBufferSignal_do_1_MC_D1_IN1,
      O => do_1_MC_D1_58
    );
  do_1_MC_D2 : X_ZERO
    port map (
      O => do_1_MC_D2_59
    );
  dip2_0_MC_Q : X_BUF
    port map (
      I => dip2_0_MC_Q_tsimrenamed_net_Q,
      O => dip2_0_MC_Q_13
    );
  dip2_0_MC_UIM : X_BUF
    port map (
      I => dip2_0_MC_Q_tsimrenamed_net_Q,
      O => dip2_0_MC_UIM_61
    );
  dip2_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip2_0_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip2_0_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip2_0_MC_Q_tsimrenamed_net_Q
    );
  dip2_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip2_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip2_0_MC_D_IN1,
      O => dip2_0_MC_D_62
    );
  dip2_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip2_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip2_0_MC_D1_IN1,
      O => dip2_0_MC_D1_63
    );
  dip2_0_MC_D2 : X_ZERO
    port map (
      O => dip2_0_MC_D2_64
    );
  dip2_1_MC_Q : X_BUF
    port map (
      I => dip2_1_MC_Q_tsimrenamed_net_Q,
      O => dip2_1_MC_Q_15
    );
  dip2_1_MC_UIM : X_BUF
    port map (
      I => dip2_1_MC_Q_tsimrenamed_net_Q,
      O => dip2_1_MC_UIM_66
    );
  dip2_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip2_1_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip2_1_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip2_1_MC_Q_tsimrenamed_net_Q
    );
  dip2_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip2_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip2_1_MC_D_IN1,
      O => dip2_1_MC_D_67
    );
  dip2_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip2_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip2_1_MC_D1_IN1,
      O => dip2_1_MC_D1_68
    );
  dip2_1_MC_D2 : X_ZERO
    port map (
      O => dip2_1_MC_D2_69
    );
  dip3_0_MC_Q : X_BUF
    port map (
      I => dip3_0_MC_Q_tsimrenamed_net_Q,
      O => dip3_0_MC_Q_17
    );
  dip3_0_MC_UIM : X_BUF
    port map (
      I => dip3_0_MC_Q_tsimrenamed_net_Q,
      O => dip3_0_MC_UIM_71
    );
  dip3_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip3_0_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip3_0_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip3_0_MC_Q_tsimrenamed_net_Q
    );
  dip3_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip3_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip3_0_MC_D_IN1,
      O => dip3_0_MC_D_72
    );
  dip3_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip3_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip3_0_MC_D1_IN1,
      O => dip3_0_MC_D1_73
    );
  dip3_0_MC_D2 : X_ZERO
    port map (
      O => dip3_0_MC_D2_74
    );
  dip3_1_MC_Q : X_BUF
    port map (
      I => dip3_1_MC_Q_tsimrenamed_net_Q,
      O => dip3_1_MC_Q_19
    );
  dip3_1_MC_UIM : X_BUF
    port map (
      I => dip3_1_MC_Q_tsimrenamed_net_Q,
      O => dip3_1_MC_UIM_76
    );
  dip3_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip3_1_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip3_1_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip3_1_MC_Q_tsimrenamed_net_Q
    );
  dip3_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip3_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip3_1_MC_D_IN1,
      O => dip3_1_MC_D_77
    );
  dip3_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip3_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip3_1_MC_D1_IN1,
      O => dip3_1_MC_D1_78
    );
  dip3_1_MC_D2 : X_ZERO
    port map (
      O => dip3_1_MC_D2_79
    );
  dip4_0_MC_Q : X_BUF
    port map (
      I => dip4_0_MC_Q_tsimrenamed_net_Q,
      O => dip4_0_MC_Q_21
    );
  dip4_0_MC_UIM : X_BUF
    port map (
      I => dip4_0_MC_Q_tsimrenamed_net_Q,
      O => dip4_0_MC_UIM_81
    );
  dip4_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip4_0_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip4_0_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip4_0_MC_Q_tsimrenamed_net_Q
    );
  dip4_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip4_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip4_0_MC_D_IN1,
      O => dip4_0_MC_D_82
    );
  dip4_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip4_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip4_0_MC_D1_IN1,
      O => dip4_0_MC_D1_83
    );
  dip4_0_MC_D2 : X_ZERO
    port map (
      O => dip4_0_MC_D2_84
    );
  dip4_1_MC_Q : X_BUF
    port map (
      I => dip4_1_MC_Q_tsimrenamed_net_Q,
      O => dip4_1_MC_Q_23
    );
  dip4_1_MC_UIM : X_BUF
    port map (
      I => dip4_1_MC_Q_tsimrenamed_net_Q,
      O => dip4_1_MC_UIM_86
    );
  dip4_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip4_1_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip4_1_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip4_1_MC_Q_tsimrenamed_net_Q
    );
  dip4_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip4_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip4_1_MC_D_IN1,
      O => dip4_1_MC_D_87
    );
  dip4_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip4_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip4_1_MC_D1_IN1,
      O => dip4_1_MC_D1_88
    );
  dip4_1_MC_D2 : X_ZERO
    port map (
      O => dip4_1_MC_D2_89
    );
  dip5_0_MC_Q : X_BUF
    port map (
      I => dip5_0_MC_Q_tsimrenamed_net_Q,
      O => dip5_0_MC_Q_25
    );
  dip5_0_MC_UIM : X_BUF
    port map (
      I => dip5_0_MC_Q_tsimrenamed_net_Q,
      O => dip5_0_MC_UIM_91
    );
  dip5_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip5_0_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip5_0_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip5_0_MC_Q_tsimrenamed_net_Q
    );
  dip5_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip5_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip5_0_MC_D_IN1,
      O => dip5_0_MC_D_92
    );
  dip5_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip5_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip5_0_MC_D1_IN1,
      O => dip5_0_MC_D1_93
    );
  dip5_0_MC_D2 : X_ZERO
    port map (
      O => dip5_0_MC_D2_94
    );
  dip5_1_MC_Q : X_BUF
    port map (
      I => dip5_1_MC_Q_tsimrenamed_net_Q,
      O => dip5_1_MC_Q_27
    );
  dip5_1_MC_UIM : X_BUF
    port map (
      I => dip5_1_MC_Q_tsimrenamed_net_Q,
      O => dip5_1_MC_UIM_96
    );
  dip5_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip5_1_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip5_1_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip5_1_MC_Q_tsimrenamed_net_Q
    );
  dip5_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip5_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip5_1_MC_D_IN1,
      O => dip5_1_MC_D_97
    );
  dip5_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip5_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip5_1_MC_D1_IN1,
      O => dip5_1_MC_D1_98
    );
  dip5_1_MC_D2 : X_ZERO
    port map (
      O => dip5_1_MC_D2_99
    );
  dip6_0_MC_Q : X_BUF
    port map (
      I => dip6_0_MC_Q_tsimrenamed_net_Q,
      O => dip6_0_MC_Q_29
    );
  dip6_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip6_0_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip6_0_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip6_0_MC_Q_tsimrenamed_net_Q
    );
  dip6_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip6_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip6_0_MC_D_IN1,
      O => dip6_0_MC_D_101
    );
  dip6_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip6_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip6_0_MC_D1_IN1,
      O => dip6_0_MC_D1_102
    );
  dip6_0_MC_D2 : X_ZERO
    port map (
      O => dip6_0_MC_D2_103
    );
  dip6_1_MC_Q : X_BUF
    port map (
      I => dip6_1_MC_Q_tsimrenamed_net_Q,
      O => dip6_1_MC_Q_31
    );
  dip6_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip6_1_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_dip6_1_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => dip6_1_MC_Q_tsimrenamed_net_Q
    );
  dip6_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip6_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip6_1_MC_D_IN1,
      O => dip6_1_MC_D_105
    );
  dip6_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip6_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip6_1_MC_D1_IN1,
      O => dip6_1_MC_D1_106
    );
  dip6_1_MC_D2 : X_ZERO
    port map (
      O => dip6_1_MC_D2_107
    );
  mrk_MC_Q : X_BUF
    port map (
      I => mrk_MC_Q_tsimrenamed_net_Q,
      O => mrk_MC_Q_37
    );
  mrk_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_mrk_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_mrk_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => mrk_MC_Q_tsimrenamed_net_Q
    );
  mrk_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_mrk_MC_D_IN0,
      I1 => NlwBufferSignal_mrk_MC_D_IN1,
      O => mrk_MC_D_109
    );
  mrk_MC_D1 : X_AND5
    port map (
      I0 => NlwBufferSignal_mrk_MC_D1_IN0,
      I1 => NlwInverterSignal_mrk_MC_D1_IN1,
      I2 => NlwInverterSignal_mrk_MC_D1_IN2,
      I3 => NlwBufferSignal_mrk_MC_D1_IN3,
      I4 => NlwInverterSignal_mrk_MC_D1_IN4,
      O => mrk_MC_D1_110
    );
  mrk_MC_D2 : X_ZERO
    port map (
      O => mrk_MC_D2_111
    );
  patternchecker_i_state_cs_FSM_FFd1 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd1_MC_Q,
      O => patternchecker_i_state_cs_FSM_FFd1_112
    );
  patternchecker_i_state_cs_FSM_FFd1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => patternchecker_i_state_cs_FSM_FFd1_MC_Q
    );
  patternchecker_i_state_cs_FSM_FFd1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D_IN0,
      I1 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D_IN1,
      O => patternchecker_i_state_cs_FSM_FFd1_MC_D_115
    );
  patternchecker_i_state_cs_FSM_FFd1_MC_D1 : X_ZERO
    port map (
      O => patternchecker_i_state_cs_FSM_FFd1_MC_D1_116
    );
  patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN3,
      O => patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_118
    );
  patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN4,
      O => patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_119
    );
  patternchecker_i_state_cs_FSM_FFd1_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_IN0,
      I1 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_IN1,
      O => patternchecker_i_state_cs_FSM_FFd1_MC_D2_117
    );
  patternchecker_i_state_cs_FSM_FFd2 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd2_MC_Q,
      O => patternchecker_i_state_cs_FSM_FFd2_113
    );
  patternchecker_i_state_cs_FSM_FFd2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_REG_IN,
      CE => Vcc_42,
      CLK => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_REG_CLK,
      SET => Gnd_41,
      RST => Gnd_41,
      O => patternchecker_i_state_cs_FSM_FFd2_MC_Q
    );
  patternchecker_i_state_cs_FSM_FFd2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D_IN0,
      I1 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D_IN1,
      O => patternchecker_i_state_cs_FSM_FFd2_MC_D_121
    );
  patternchecker_i_state_cs_FSM_FFd2_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN0,
      I1 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN1,
      I2 => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN2,
      O => patternchecker_i_state_cs_FSM_FFd2_MC_D1_122
    );
  patternchecker_i_state_cs_FSM_FFd2_MC_D2 : X_ZERO
    port map (
      O => patternchecker_i_state_cs_FSM_FFd2_MC_D2_123
    );
  NlwBufferBlock_dip1_0_MC_REG_IN : X_BUF
    port map (
      I => dip1_0_MC_D_40,
      O => NlwBufferSignal_dip1_0_MC_REG_IN
    );
  NlwBufferBlock_dip1_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip1_0_MC_REG_CLK
    );
  NlwBufferBlock_dip1_0_MC_D_IN0 : X_BUF
    port map (
      I => dip1_0_MC_D1_43,
      O => NlwBufferSignal_dip1_0_MC_D_IN0
    );
  NlwBufferBlock_dip1_0_MC_D_IN1 : X_BUF
    port map (
      I => dip1_0_MC_D2_44,
      O => NlwBufferSignal_dip1_0_MC_D_IN1
    );
  NlwBufferBlock_dip1_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip1_0_MC_D1_IN0
    );
  NlwBufferBlock_dip1_0_MC_D1_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_45,
      O => NlwBufferSignal_dip1_0_MC_D1_IN1
    );
  NlwBufferBlock_do_0_MC_REG_IN : X_BUF
    port map (
      I => do_0_MC_D_47,
      O => NlwBufferSignal_do_0_MC_REG_IN
    );
  NlwBufferBlock_do_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_do_0_MC_REG_CLK
    );
  NlwBufferBlock_do_0_MC_D_IN0 : X_BUF
    port map (
      I => do_0_MC_D1_48,
      O => NlwBufferSignal_do_0_MC_D_IN0
    );
  NlwBufferBlock_do_0_MC_D_IN1 : X_BUF
    port map (
      I => do_0_MC_D2_49,
      O => NlwBufferSignal_do_0_MC_D_IN1
    );
  NlwBufferBlock_do_0_MC_D1_IN0 : X_BUF
    port map (
      I => di_0_II_UIM_3,
      O => NlwBufferSignal_do_0_MC_D1_IN0
    );
  NlwBufferBlock_do_0_MC_D1_IN1 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_do_0_MC_D1_IN1
    );
  NlwBufferBlock_dip1_1_MC_REG_IN : X_BUF
    port map (
      I => dip1_1_MC_D_52,
      O => NlwBufferSignal_dip1_1_MC_REG_IN
    );
  NlwBufferBlock_dip1_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip1_1_MC_REG_CLK
    );
  NlwBufferBlock_dip1_1_MC_D_IN0 : X_BUF
    port map (
      I => dip1_1_MC_D1_53,
      O => NlwBufferSignal_dip1_1_MC_D_IN0
    );
  NlwBufferBlock_dip1_1_MC_D_IN1 : X_BUF
    port map (
      I => dip1_1_MC_D2_54,
      O => NlwBufferSignal_dip1_1_MC_D_IN1
    );
  NlwBufferBlock_dip1_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip1_1_MC_D1_IN0
    );
  NlwBufferBlock_dip1_1_MC_D1_IN1 : X_BUF
    port map (
      I => do_1_MC_UIM_55,
      O => NlwBufferSignal_dip1_1_MC_D1_IN1
    );
  NlwBufferBlock_do_1_MC_REG_IN : X_BUF
    port map (
      I => do_1_MC_D_57,
      O => NlwBufferSignal_do_1_MC_REG_IN
    );
  NlwBufferBlock_do_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_do_1_MC_REG_CLK
    );
  NlwBufferBlock_do_1_MC_D_IN0 : X_BUF
    port map (
      I => do_1_MC_D1_58,
      O => NlwBufferSignal_do_1_MC_D_IN0
    );
  NlwBufferBlock_do_1_MC_D_IN1 : X_BUF
    port map (
      I => do_1_MC_D2_59,
      O => NlwBufferSignal_do_1_MC_D_IN1
    );
  NlwBufferBlock_do_1_MC_D1_IN0 : X_BUF
    port map (
      I => di_1_II_UIM_5,
      O => NlwBufferSignal_do_1_MC_D1_IN0
    );
  NlwBufferBlock_do_1_MC_D1_IN1 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_do_1_MC_D1_IN1
    );
  NlwBufferBlock_dip2_0_MC_REG_IN : X_BUF
    port map (
      I => dip2_0_MC_D_62,
      O => NlwBufferSignal_dip2_0_MC_REG_IN
    );
  NlwBufferBlock_dip2_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip2_0_MC_REG_CLK
    );
  NlwBufferBlock_dip2_0_MC_D_IN0 : X_BUF
    port map (
      I => dip2_0_MC_D1_63,
      O => NlwBufferSignal_dip2_0_MC_D_IN0
    );
  NlwBufferBlock_dip2_0_MC_D_IN1 : X_BUF
    port map (
      I => dip2_0_MC_D2_64,
      O => NlwBufferSignal_dip2_0_MC_D_IN1
    );
  NlwBufferBlock_dip2_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip2_0_MC_D1_IN0
    );
  NlwBufferBlock_dip2_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip1_0_MC_UIM_39,
      O => NlwBufferSignal_dip2_0_MC_D1_IN1
    );
  NlwBufferBlock_dip2_1_MC_REG_IN : X_BUF
    port map (
      I => dip2_1_MC_D_67,
      O => NlwBufferSignal_dip2_1_MC_REG_IN
    );
  NlwBufferBlock_dip2_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip2_1_MC_REG_CLK
    );
  NlwBufferBlock_dip2_1_MC_D_IN0 : X_BUF
    port map (
      I => dip2_1_MC_D1_68,
      O => NlwBufferSignal_dip2_1_MC_D_IN0
    );
  NlwBufferBlock_dip2_1_MC_D_IN1 : X_BUF
    port map (
      I => dip2_1_MC_D2_69,
      O => NlwBufferSignal_dip2_1_MC_D_IN1
    );
  NlwBufferBlock_dip2_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip2_1_MC_D1_IN0
    );
  NlwBufferBlock_dip2_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip1_1_MC_UIM_51,
      O => NlwBufferSignal_dip2_1_MC_D1_IN1
    );
  NlwBufferBlock_dip3_0_MC_REG_IN : X_BUF
    port map (
      I => dip3_0_MC_D_72,
      O => NlwBufferSignal_dip3_0_MC_REG_IN
    );
  NlwBufferBlock_dip3_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip3_0_MC_REG_CLK
    );
  NlwBufferBlock_dip3_0_MC_D_IN0 : X_BUF
    port map (
      I => dip3_0_MC_D1_73,
      O => NlwBufferSignal_dip3_0_MC_D_IN0
    );
  NlwBufferBlock_dip3_0_MC_D_IN1 : X_BUF
    port map (
      I => dip3_0_MC_D2_74,
      O => NlwBufferSignal_dip3_0_MC_D_IN1
    );
  NlwBufferBlock_dip3_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip3_0_MC_D1_IN0
    );
  NlwBufferBlock_dip3_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip2_0_MC_UIM_61,
      O => NlwBufferSignal_dip3_0_MC_D1_IN1
    );
  NlwBufferBlock_dip3_1_MC_REG_IN : X_BUF
    port map (
      I => dip3_1_MC_D_77,
      O => NlwBufferSignal_dip3_1_MC_REG_IN
    );
  NlwBufferBlock_dip3_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip3_1_MC_REG_CLK
    );
  NlwBufferBlock_dip3_1_MC_D_IN0 : X_BUF
    port map (
      I => dip3_1_MC_D1_78,
      O => NlwBufferSignal_dip3_1_MC_D_IN0
    );
  NlwBufferBlock_dip3_1_MC_D_IN1 : X_BUF
    port map (
      I => dip3_1_MC_D2_79,
      O => NlwBufferSignal_dip3_1_MC_D_IN1
    );
  NlwBufferBlock_dip3_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip3_1_MC_D1_IN0
    );
  NlwBufferBlock_dip3_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip2_1_MC_UIM_66,
      O => NlwBufferSignal_dip3_1_MC_D1_IN1
    );
  NlwBufferBlock_dip4_0_MC_REG_IN : X_BUF
    port map (
      I => dip4_0_MC_D_82,
      O => NlwBufferSignal_dip4_0_MC_REG_IN
    );
  NlwBufferBlock_dip4_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip4_0_MC_REG_CLK
    );
  NlwBufferBlock_dip4_0_MC_D_IN0 : X_BUF
    port map (
      I => dip4_0_MC_D1_83,
      O => NlwBufferSignal_dip4_0_MC_D_IN0
    );
  NlwBufferBlock_dip4_0_MC_D_IN1 : X_BUF
    port map (
      I => dip4_0_MC_D2_84,
      O => NlwBufferSignal_dip4_0_MC_D_IN1
    );
  NlwBufferBlock_dip4_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip4_0_MC_D1_IN0
    );
  NlwBufferBlock_dip4_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip3_0_MC_UIM_71,
      O => NlwBufferSignal_dip4_0_MC_D1_IN1
    );
  NlwBufferBlock_dip4_1_MC_REG_IN : X_BUF
    port map (
      I => dip4_1_MC_D_87,
      O => NlwBufferSignal_dip4_1_MC_REG_IN
    );
  NlwBufferBlock_dip4_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip4_1_MC_REG_CLK
    );
  NlwBufferBlock_dip4_1_MC_D_IN0 : X_BUF
    port map (
      I => dip4_1_MC_D1_88,
      O => NlwBufferSignal_dip4_1_MC_D_IN0
    );
  NlwBufferBlock_dip4_1_MC_D_IN1 : X_BUF
    port map (
      I => dip4_1_MC_D2_89,
      O => NlwBufferSignal_dip4_1_MC_D_IN1
    );
  NlwBufferBlock_dip4_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip4_1_MC_D1_IN0
    );
  NlwBufferBlock_dip4_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip3_1_MC_UIM_76,
      O => NlwBufferSignal_dip4_1_MC_D1_IN1
    );
  NlwBufferBlock_dip5_0_MC_REG_IN : X_BUF
    port map (
      I => dip5_0_MC_D_92,
      O => NlwBufferSignal_dip5_0_MC_REG_IN
    );
  NlwBufferBlock_dip5_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip5_0_MC_REG_CLK
    );
  NlwBufferBlock_dip5_0_MC_D_IN0 : X_BUF
    port map (
      I => dip5_0_MC_D1_93,
      O => NlwBufferSignal_dip5_0_MC_D_IN0
    );
  NlwBufferBlock_dip5_0_MC_D_IN1 : X_BUF
    port map (
      I => dip5_0_MC_D2_94,
      O => NlwBufferSignal_dip5_0_MC_D_IN1
    );
  NlwBufferBlock_dip5_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip5_0_MC_D1_IN0
    );
  NlwBufferBlock_dip5_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip4_0_MC_UIM_81,
      O => NlwBufferSignal_dip5_0_MC_D1_IN1
    );
  NlwBufferBlock_dip5_1_MC_REG_IN : X_BUF
    port map (
      I => dip5_1_MC_D_97,
      O => NlwBufferSignal_dip5_1_MC_REG_IN
    );
  NlwBufferBlock_dip5_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip5_1_MC_REG_CLK
    );
  NlwBufferBlock_dip5_1_MC_D_IN0 : X_BUF
    port map (
      I => dip5_1_MC_D1_98,
      O => NlwBufferSignal_dip5_1_MC_D_IN0
    );
  NlwBufferBlock_dip5_1_MC_D_IN1 : X_BUF
    port map (
      I => dip5_1_MC_D2_99,
      O => NlwBufferSignal_dip5_1_MC_D_IN1
    );
  NlwBufferBlock_dip5_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip5_1_MC_D1_IN0
    );
  NlwBufferBlock_dip5_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip4_1_MC_UIM_86,
      O => NlwBufferSignal_dip5_1_MC_D1_IN1
    );
  NlwBufferBlock_dip6_0_MC_REG_IN : X_BUF
    port map (
      I => dip6_0_MC_D_101,
      O => NlwBufferSignal_dip6_0_MC_REG_IN
    );
  NlwBufferBlock_dip6_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip6_0_MC_REG_CLK
    );
  NlwBufferBlock_dip6_0_MC_D_IN0 : X_BUF
    port map (
      I => dip6_0_MC_D1_102,
      O => NlwBufferSignal_dip6_0_MC_D_IN0
    );
  NlwBufferBlock_dip6_0_MC_D_IN1 : X_BUF
    port map (
      I => dip6_0_MC_D2_103,
      O => NlwBufferSignal_dip6_0_MC_D_IN1
    );
  NlwBufferBlock_dip6_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip6_0_MC_D1_IN0
    );
  NlwBufferBlock_dip6_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip5_0_MC_UIM_91,
      O => NlwBufferSignal_dip6_0_MC_D1_IN1
    );
  NlwBufferBlock_dip6_1_MC_REG_IN : X_BUF
    port map (
      I => dip6_1_MC_D_105,
      O => NlwBufferSignal_dip6_1_MC_REG_IN
    );
  NlwBufferBlock_dip6_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip6_1_MC_REG_CLK
    );
  NlwBufferBlock_dip6_1_MC_D_IN0 : X_BUF
    port map (
      I => dip6_1_MC_D1_106,
      O => NlwBufferSignal_dip6_1_MC_D_IN0
    );
  NlwBufferBlock_dip6_1_MC_D_IN1 : X_BUF
    port map (
      I => dip6_1_MC_D2_107,
      O => NlwBufferSignal_dip6_1_MC_D_IN1
    );
  NlwBufferBlock_dip6_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip6_1_MC_D1_IN0
    );
  NlwBufferBlock_dip6_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip5_1_MC_UIM_96,
      O => NlwBufferSignal_dip6_1_MC_D1_IN1
    );
  NlwBufferBlock_mrk_MC_REG_IN : X_BUF
    port map (
      I => mrk_MC_D_109,
      O => NlwBufferSignal_mrk_MC_REG_IN
    );
  NlwBufferBlock_mrk_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_mrk_MC_REG_CLK
    );
  NlwBufferBlock_mrk_MC_D_IN0 : X_BUF
    port map (
      I => mrk_MC_D1_110,
      O => NlwBufferSignal_mrk_MC_D_IN0
    );
  NlwBufferBlock_mrk_MC_D_IN1 : X_BUF
    port map (
      I => mrk_MC_D2_111,
      O => NlwBufferSignal_mrk_MC_D_IN1
    );
  NlwBufferBlock_mrk_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_mrk_MC_D1_IN0
    );
  NlwBufferBlock_mrk_MC_D1_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_45,
      O => NlwBufferSignal_mrk_MC_D1_IN1
    );
  NlwBufferBlock_mrk_MC_D1_IN2 : X_BUF
    port map (
      I => do_1_MC_UIM_55,
      O => NlwBufferSignal_mrk_MC_D1_IN2
    );
  NlwBufferBlock_mrk_MC_D1_IN3 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd1_112,
      O => NlwBufferSignal_mrk_MC_D1_IN3
    );
  NlwBufferBlock_mrk_MC_D1_IN4 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd2_113,
      O => NlwBufferSignal_mrk_MC_D1_IN4
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_REG_IN : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd1_MC_D_115,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_REG_IN
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_REG_CLK
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D_IN0 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd1_MC_D1_116,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D_IN0
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D_IN1 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd1_MC_D2_117,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D_IN1
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_45,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => do_1_MC_UIM_55,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd2_113,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_45,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => do_1_MC_UIM_55,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd1_112,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd2_113,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_IN0 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_0_118,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_IN0
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_IN1 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_119,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_IN1
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd2_MC_REG_IN : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd2_MC_D_121,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_REG_IN
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_REG_CLK
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd2_MC_D_IN0 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd2_MC_D1_122,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D_IN0
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd2_MC_D_IN1 : X_BUF
    port map (
      I => patternchecker_i_state_cs_FSM_FFd2_MC_D2_123,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D_IN1
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN0
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_45,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN1
    );
  NlwBufferBlock_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN2 : X_BUF
    port map (
      I => do_1_MC_UIM_55,
      O => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd2_MC_D1_IN2
    );
  NlwInverterBlock_mrk_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_mrk_MC_D1_IN1,
      O => NlwInverterSignal_mrk_MC_D1_IN1
    );
  NlwInverterBlock_mrk_MC_D1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_mrk_MC_D1_IN2,
      O => NlwInverterSignal_mrk_MC_D1_IN2
    );
  NlwInverterBlock_mrk_MC_D1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_mrk_MC_D1_IN4,
      O => NlwInverterSignal_mrk_MC_D1_IN4
    );
  NlwInverterBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_patternchecker_i_state_cs_FSM_FFd1_MC_D2_PT_1_IN2
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure_mealy;

