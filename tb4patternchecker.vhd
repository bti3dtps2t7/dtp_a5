library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;


entity tb4patternchecker is
end entity tb4patternchecker;



architecture beh of tb4patternchecker is

    signal txData_s : std_logic_vector ( 1 downto 0);
    signal clk_s : std_logic;
    signal nres_s : std_logic;
    signal do_s : std_logic_vector ( 1 downto 0);
    signal mrk_s : std_logic;

    signal dip1_s : std_logic_vector( 1 downto 0 );
    signal dip2_s : std_logic_vector( 1 downto 0 );
    signal dip3_s : std_logic_vector( 1 downto 0 );
    signal dip4_s : std_logic_vector( 1 downto 0 );
    signal dip5_s : std_logic_vector( 1 downto 0 );
    signal dip6_s : std_logic_vector( 1 downto 0 );


    component sg is
        port(
                txData : out std_logic_vector( 1 downto 0 );   -- TxData send to DUT
                clk    : out std_logic;                        -- CLocK
                nres   : out std_logic                         -- Not RESet ; low active reset
            );--]port
    end component sg;
    for all : sg use entity work.sg( beh );

    component dut is
        port (
                dip1 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 1 clock cycle (for debugging purpose only)
                dip2 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 2 clock cycles (for debugging purpose only)
                dip3 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 3 clock cycles (for debugging purpose only)
                dip4 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 4 clock cycles (for debugging purpose only)
                dip5 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 5 clock cycles (for debugging purpose only)
                dip6 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 6 clock cycles (for debugging purpose only)
                do : out std_logic_vector( 1 downto 0 ); -- Data Out
                mrk : out std_logic; -- MaRK of 1st data following pattern
                --
                di : in std_logic_vector( 1 downto 0 ); -- Data In
                --
                clk : in std_logic; -- CLocK
                nres : in std_logic -- Not RESet ; low active reset
            );
    end component dut;
    for all : dut use entity work.dut( beh );

begin
    sg_i : sg
        port map (
            txData => txData_s,
            clk => clk_s,
            nres => nres_s
        )--]port
    ;--]sg_i

    patternchecker_i : dut
        port map (
            di => txData_s,
            clk => clk_s,
            nres => nres_s,
            do => do_s,
            mrk => mrk_s,

            dip1 => dip1_s,
            dip2 => dip2_s,
            dip3 => dip3_s,
            dip4 => dip4_s,
            dip5 => dip5_s,
            dip6 => dip6_s

        )--]port
    ;--]sg_i

end architecture beh;

